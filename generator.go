package uuid

import uuidg "github.com/google/uuid"

type Generator interface {
	New() UUID
}

type GeneratorImpl struct{}

// NewGeneratorImpl is a constrictor.
func NewGeneratorImpl() *GeneratorImpl {
	return &GeneratorImpl{}
}

// New generates new uuid.
func (g GeneratorImpl) New() UUID {
	return UUID(uuidg.New())
}
