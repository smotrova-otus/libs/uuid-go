module gitlab.com/smotrova-otus/libs/uuid-go

go 1.13

require (
	github.com/google/uuid v1.1.2
	github.com/stretchr/testify v1.6.1
)
