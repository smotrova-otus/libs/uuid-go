package uuid

import (
	"bytes"
	"database/sql/driver"
	google "github.com/google/uuid"
)

type UUID google.UUID

var Nil = UUID(google.Nil)

// Parse converts string to uuid.
func Parse(value string) (UUID, error) {
	id, err := google.Parse(value)
	if err != nil {
		return UUID(google.Nil), err
	}
	return UUID(id), nil
}

// Must returns uuid if err is nil and panics otherwise.
func Must(uuid UUID, err error) UUID {
	return UUID(
		google.Must(google.UUID(uuid), err),
	)
}

// Must returns uuid if err is nil and panics otherwise.
// NewRandom returns a Random (Version 4) UUID.
//
// The strength of the UUIDs is based on the strength of the crypto/rand
// package.
//
// A note about uniqueness derived from the UUID Wikipedia entry:
//
//  Randomly generated UUIDs have 122 random bits.  One's annual risk of being
//  hit by a meteorite is estimated to be one chance in 17 billion, that
//  means the probability is about 0.00000000006 (6 × 10−11),
//  equivalent to the odds of creating a few tens of trillions of UUIDs in a
//  year and having one duplicate.
func NewRandom() (UUID, error) {
	uuid, err := google.NewRandom()
	return UUID(uuid), err
}

// MustParse is like Parse but panics if the string cannot be parsed.
func MustParse(s string) UUID {
	uuid, err := Parse(s)
	if err != nil {
		panic(`uuid: Parse(` + s + `): ` + err.Error())
	}
	return uuid
}

// Scan implements sql.Scanner so UUIDs can be read from databases transparently
// Currently, database types that map to string and []byte are supported. Please
// consult database-specific driver documentation for matching types.
func (id *UUID) Scan(src interface{}) error {
	return (*google.UUID)(id).Scan(src)
}

// Value implements sql.Valuer so that UUIDs can be written to databases
// transparently. Currently, UUIDs map to strings. Please consult
// database-specific driver documentation for matching types.
func (id UUID) Value() (driver.Value, error) {
	return google.UUID(id).Value()
}

// MarshalText implements encoding.TextMarshaler.
func (id UUID) MarshalText() ([]byte, error) {
	return google.UUID(id).MarshalText()
}

// UnmarshalText implements encoding.TextUnmarshaler.
func (id *UUID) UnmarshalText(data []byte) error {
	u := (*google.UUID)(id)
	return u.UnmarshalText(data)
}

// MarshalBinary implements encoding.BinaryMarshaler.
func (id UUID) MarshalBinary() ([]byte, error) {
	return google.UUID(id).MarshalBinary()
}

// UnmarshalBinary implements encoding.BinaryUnmarshaler.
func (id *UUID) UnmarshalBinary(data []byte) error {
	u := google.UUID(*id)
	return u.UnmarshalBinary(data)
}

// String returns the string form of uuid, xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
// , or "" if uuid is invalid.
func (id UUID) String() string {
	u := google.UUID(id)
	return u.String()
}

type UUIDs []UUID

func (u UUIDs) Len() int {
	return len(u)
}

func (u UUIDs) Less(i, j int) bool {
	return bytes.Compare(u[i][:], u[j][:]) == -1
}

func (u UUIDs) Swap(i, j int) {
	u[i], u[j] = u[j], u[i]
}
